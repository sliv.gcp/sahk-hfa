import './util/bootstrap';
import { Container } from 'inversify';
import { GSheetService } from './service/google/g-sheet-service';
import { Inject } from './constants/tokens';
import { readFile } from 'fs';
import { resolve } from 'path';

async function start() {
    const container = new Container();
    container.bind(GSheetService).toSelf().inSingletonScope();
    const clientSecretFilename =
        'client_secret_735574818561-sp90l7mb7clof7q0rat5ndku2sk0l780.apps.googleusercontent.com.json';
    const credential = await new Promise((r, d) => {
        const secretPath = resolve(__dirname, '../.credentials/', clientSecretFilename);
        readFile(secretPath, 'utf-8',
            (err, content) => {
                if (err) return d(err);
                r(JSON.parse(content));
            });
    });
    container.bind(Inject.gsheetCredential.token).toConstantValue(credential);
    const gsheetService = container.get(GSheetService);
    gsheetService.auth();
}

start()
    .catch(console.error);