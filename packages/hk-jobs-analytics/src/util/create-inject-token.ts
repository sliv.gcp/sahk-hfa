import { inject, injectable } from 'inversify';

export function createInjectToken(id: string) {
    const token = Symbol(id);
    return Object.assign(inject(token), { token });
}