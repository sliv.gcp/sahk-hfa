import { format, parse, UrlObject } from 'url';

function merge(...args) {
    return Object.assign({}, ...args);
}

export function UrlBuilder(opt: UrlObject | string) {
    const urlObject = (typeof opt === 'string' ? parse(opt, true) : opt);
    return {
        ...urlObject,
        toString() {
            return format(urlObject);
        },
        merge(extendOpt: UrlObject) {
            return merge(urlObject, extendOpt);
        },
        addPathname(path: string) {
            return UrlBuilder(merge(urlObject, { pathname: (urlObject.pathname || '') + path }));
        },
        addQuery(nextQuery) {
            const query = merge(urlObject.query || {}, nextQuery);
            return UrlBuilder(merge(urlObject, { query: query }));
        },
    };
}

export function RequestBuilder() {
    return {
        execute() {

        },
    };
}