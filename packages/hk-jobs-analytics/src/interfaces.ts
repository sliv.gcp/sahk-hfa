import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

export namespace SAHK_HFA {


    export namespace Entities {
        export interface UrlResponse {
            timestamp: Date;
        }

        export interface JobQueryOpts {
            functions: string[]
            industries: string[]
            experience: number;
            page: number
        }

        export interface JobEntry {
            refId: string;
            sourceSite: string;
            title: string;
            locationName: string;
            timestamp: Date;
            email: string;
            experience: number;
        }

        export interface JobFunction {
            name: string;
            refId: string;
            siteName: string;
        }

        export interface JobIndustry {
            name: string;
            refId: string;
            siteName: string;
        }

        export interface GeoLocation {
            name: string;
            lat: number;
            lng: number;
        }

        export interface ClientEntry {
            name: string;
            geneder: string;
        }
    }

    export namespace Enums {
        export enum LogLevel {
            info,
            debug,
            warn,
            error,
            trace
        }

        export enum EntryLevel {
            entry,
            middle,
            senior,
            top
        }

        export enum EducationLevel {
            none,
            mid,
            high,
            college,
            master,
            phd
        }
    }

    export interface VoidFn {
        (...args): void
    }

    export namespace Service {
        import LogLevel = SAHK_HFA.Enums.LogLevel;

        export interface CredentialService {
            gsheetClientSecret: object;
            gdatastoreServiceSecret: object;
        }

        export interface HttpService {
            html: Observable<{ body: string }>
            json: Observable<{ body: object }>
        }

        export interface Logger {
            debug: VoidFn;
            info: VoidFn;
            warn: VoidFn;
            error: VoidFn;
            trace: VoidFn;
        }

        export interface LogService {
            $: Subject<{ level: LogLevel, from: any, meta: any[], args: any[] }>;

            getLogger(arg: any): Logger;
        }

        export interface FsService {
            getScope(): {
                readFile: Promise<String>,
                writeFile: Promise<String>,
            }
        }
    }
}