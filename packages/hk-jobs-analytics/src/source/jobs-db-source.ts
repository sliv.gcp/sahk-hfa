import { injectable } from 'inversify';
import { SAHK_HFA } from '../interfaces';
import { UrlBuilder } from '../util/url-extendable-format';
import JobQueryOpts = SAHK_HFA.Entities.JobQueryOpts;
import { UrlObject } from 'url';
import { LogService } from '../service/log-service';
import { HttpService } from '../service/http-service';

@injectable()
export class JobsDbSource {
    private logger: SAHK_HFA.Service.Logger;
    private fetch: typeof fetch;

    constructor(
        logService: LogService,
        httpService: HttpService,
    ) {
        this.logger = logService.getLogger('source:jobsdb');
        this.fetch = httpService.getFetch('source:jobsdb');
    }

    root = UrlBuilder({
        protocol: 'http',
        host: 'hk.jobsdb.com',
    });

    async query(opt: Partial<JobQueryOpts>) {
        const { info } = this.logger;
        info(this.root.addPathname('/').toString());
    }

    async getFunctionMapPage() {
        const url = this.root.addPathname('/hk/en/browse');
        await this.fetch(url.toString());
    }

    async detail(url: string) {

    }
}