import '../util/bootstrap';
import { blue, cyan, green, grey, red, underline, yellow } from 'chalk';
import * as clear from 'clear';
import { Container, ContainerModule } from 'inversify';
import { basename } from 'path';
import { SAHK_HFA } from '../interfaces';
import { HttpService } from '../service/http-service';
import { LogService } from '../service/log-service';
import { JobsDbSource } from '../source/jobs-db-source';
import { SqliteService } from '../service/sql/sql-service';
import { Inject } from '../constants/tokens';
import { UrlResponseEntity } from '../entities/url-response';
import { PathService } from '../service/paths-service';
import { matchHarness } from 'ramda-match';
import * as prettyjson from 'prettyjson';
import LogLevel = SAHK_HFA.Enums.LogLevel;
import { JobEntryEntity } from '../entities/job-entry.entity';


const containerModules = new ContainerModule(bind => {
    function selfBind(t) {
        bind(t).toSelf().inSingletonScope();
    }

    const serivces = [
        LogService,
        JobsDbSource, HttpService, SqliteService, PathService];
    serivces
        .forEach(selfBind);
    bind(Inject.typeormEntities.token)
        .toConstantValue([UrlResponseEntity, JobEntryEntity]);
});

const LogLevelColor = {
    [LogLevel.debug]: blue('D'),
    [LogLevel.info]: green('I'),
    [LogLevel.warn]: yellow('W'),
    [LogLevel.error]: red('E'),
    [LogLevel.trace]: grey('T'),
};

function simpleConsoleLog({ from, level, args }) {
    const logArgs = args.reduce((pr, arg) => {
        return matchHarness(m => {
            const whenObj = m('object');
            whenObj(() => {
                const objArg = prettyjson.render(arg)
                    .split('\n')
                    .map((s, i) => i == 0 ? s : ` ${s}`)
                    .join('\n');
                return pr.concat('\n')
                    .concat(objArg);
            });
            m(() => true)(() => pr.concat(arg));
        })(typeof arg);
    }, []);

    console.log(LogLevelColor[level], underline(cyan(from)), ...logArgs, '\n');
}

const container = new Container();

async function init() {
    clear();
    container.load(containerModules);
    const logService = container.get(LogService);
    const { info, error } = logService.getLogger(basename(__filename));
    logService.$.subscribe(simpleConsoleLog);
    // setup logger
    info('bootstrap', `${Date.now()}`);
    process.on('uncaughtException', error);
    process.on('exit', (code) => info('exited ', code));

    // setup db
    const sqliteService = container.get(SqliteService);
    await sqliteService.init();
    info('bootstrap successful', `${Date.now()}`);
}

async function main() {
    await init();
    const logService = container.get(LogService);
    const { info, error } = logService.getLogger(basename(__filename));
    const jobsDbSource = container.get(JobsDbSource);
    const { repo: { UrlResponseRepo }, conn } = container.get(SqliteService);
//    await conn.transaction(async (em) => {
//        const urlResponseEntity = new UrlResponseEntity();
//        urlResponseEntity.setWithUrl('https://google.com/google.jpg');
//        urlResponseEntity.body = 'somethings';
//        await em.save(urlResponseEntity);
//    });
//    await jobsDbSource.getFunctionMapPage();
}

main();