import { Entity } from 'typeorm/decorator/entity/Entity';
import { Column, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import { SAHK_HFA } from '../interfaces';
import Entities = SAHK_HFA.Entities;

const NullableTextColumn = Column('text', { nullable: true });
const TextColumn = Column('text');

@Entity()
export class JobEntryEntity implements Entities.JobEntry {
    static Builder = () => {

    };

    @PrimaryGeneratedColumn()
    id: number;
    @TextColumn
    refId: string;
    @TextColumn
    sourceSite: string;
    @TextColumn
    title: string;

    @NullableTextColumn
    locationName: string;

    timestamp: Date = new Date();
    @NullableTextColumn
    email: string;

    experience: number;

}