import { Entity } from 'typeorm/decorator/entity/Entity';
import { Interfaces } from 'cmd-template/interface';
import { SAHK_HFA } from '../interfaces';
import { Column, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import { format, parse } from 'url';
import { RepositoryFactory } from 'typeorm/repository/RepositoryFactory';

@Entity('url_response')
export class UrlResponseEntity implements SAHK_HFA.Entities.UrlResponse {
    static Builder = (opt) => {

    };
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    host: string;
    @Column()
    pathname: string;
    @Column('date')
    timestamp: Date = new Date();
    @Column('text', { nullable: true })
    headers: string;
    @Column()
    body: string;

    setWithUrl(url: string) {
        Object.assign(this, parse(url));
    }
}