import { createInjectToken } from '../util/create-inject-token';

export const Inject = {
    gsheetCredential: createInjectToken('gsheetCredential'),
    typeormEntities: createInjectToken('typeorm-entities'),
};