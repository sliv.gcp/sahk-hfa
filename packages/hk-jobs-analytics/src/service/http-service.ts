import { injectable, optional } from 'inversify';
import { LogService } from './log-service';
import * as fetch from 'isomorphic-fetch';
import { SAHK_HFA } from '../interfaces';
import Logger = SAHK_HFA.Service.Logger;

export function identity(i) {
    return i;
}

@injectable()
export class HttpService {
    logger: Logger = this.logService.getLogger('http');

    constructor(
        private logService: LogService,
        @optional()
        private httpRequestInterceptors = identity,
        @optional()
        private httpResponseInterceptors = identity,
    ) {

    }

    getFetch(src: string) {
        const { info, error } = this.logService.getLogger(`http->${src}`);
        return (input: RequestInfo, init?: RequestInit) => {
            info((init && init.method || 'GET'), input, init);
            const task = fetch(input, init);
            const onfulfilled = (resp: Response) => {
                if (resp.status > 200) {
                    error(resp.status, resp.url);
                } else {
                    info(resp.status, resp.url);
                }
                return resp;
            };
            return task.then(onfulfilled);
        };
    }
}