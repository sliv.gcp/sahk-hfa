import { injectable } from 'inversify';
import { readFile, writeFile } from 'fs';
import { resolve } from 'path';

function NodePromisify(target) {
    return (...args) => new Promise((r, d) => target(...args, (e, s) => e ? d(e) : r(s)));
}

const readFileAsync = NodePromisify(readFile);

@injectable()
export class FsService {
    getFsWithScope(scope: string) {
        const r = (...args) => resolve(scope, ...args);
        return {
            readFile(path: string) {

            },
            writeFile(path: string, content: string) {

            },
        };
    }
}