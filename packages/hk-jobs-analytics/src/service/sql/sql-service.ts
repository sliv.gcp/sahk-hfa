import { Inject } from '../../constants/tokens';
import { Connection, ConnectionOptions, createConnection, Repository } from 'typeorm';
import { PathService } from '../paths-service';
import { UrlResponseEntity } from '../../entities/url-response';
import { LogService } from '../log-service';
import { SAHK_HFA } from '../../interfaces';
import { injectable } from 'inversify';
import { Logger } from 'typeorm/logger/Logger';

@injectable()
export class SqliteService {
    logger: SAHK_HFA.Service.Logger;

    constructor(
        private logService: LogService,
        @Inject.typeormEntities
        private entities: any[],
        private pathService: PathService,
    ) {
        this.logger = logService.getLogger('sqlite');
    }

    conn: Connection;
    repo: {
        UrlResponseRepo: Repository<UrlResponseEntity>
    };

    async init() {
        const { logger: { info } } = this;
        const connectionOpt = {
            type: 'sqlite',
            name: 'default',
            database: this.pathService.sqlitePath,
            autoSchemaSync: true,
            entities: this.entities,
            logging: ['query'],
        } as ConnectionOptions & { storage? };
        info('initialize type orm with', connectionOpt);
        this.conn = await createConnection(connectionOpt);
        this.repo = {
            UrlResponseRepo: this.conn.getRepository(UrlResponseEntity),
        };
    }
}