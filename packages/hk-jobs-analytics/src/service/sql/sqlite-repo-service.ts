import { injectable } from 'inversify';
import { SqliteService } from './sql-service';
import { UrlResponseEntity } from '../../entities/url-response';
import { Repository } from 'typeorm';

@injectable()
export class SqliteRepoService {
    UrlResponseRepo: Repository<UrlResponseEntity>;

    constructor(
        sqliteService: SqliteService,
    ) {
        this.UrlResponseRepo = sqliteService.conn.getRepository(UrlResponseEntity);
    }
}