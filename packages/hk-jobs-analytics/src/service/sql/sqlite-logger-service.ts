import { injectable } from 'inversify';
import { Logger } from 'typeorm/logger/Logger';
import { QueryRunner } from 'typeorm';
import { LogService } from '../log-service';
import { SAHK_HFA } from '../../interfaces';

@injectable()
export class SqliteLogger implements Logger {
    logger: SAHK_HFA.Service.Logger;

    constructor(
        logService: LogService,
    ) {
        this.logger = logService.getLogger('typeorm');
    }

    logQuery(query: string, parameters?: any[], queryRunner?: QueryRunner): any {
        const { info } = this.logger;
        return info(query, parameters);
    }

    logQueryError(error: string, query: string, parameters?: any[], queryRunner?: QueryRunner): any {
        return undefined;
    }

    logQuerySlow(time: number, query: string, parameters?: any[], queryRunner?: QueryRunner): any {
        return undefined;
    }

    logSchemaBuild(message: string, queryRunner?: QueryRunner): any {
        return undefined;
    }

    log(level, message: any, queryRunner?: QueryRunner): any {
        return this.logger[level](message);
    }

}