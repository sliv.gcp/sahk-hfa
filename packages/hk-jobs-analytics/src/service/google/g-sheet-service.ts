import { injectable } from 'inversify';
import { readFile, writeFile } from 'fs';
import { Inject } from '../../constants/tokens';
import GoogleAuth = require('google-auth-library');
import google = require('googleapis');
import { sh } from 'cmd-template';
import * as readline from 'readline';
import { createDeflateRaw } from 'zlib';

@injectable()
export class GSheetService {

    constructor(
        @Inject.gsheetCredential
        private gsheetCredential: object,
    ) {

    }

    async auth() {
        const { client_id, client_secret, redirect_uris: [redirectUrl] } = this.gsheetCredential['installed'];
        const auth = new GoogleAuth();
        const oauthClient = new auth.OAuth2(client_id, client_secret, redirectUrl);
        const url = oauthClient.generateAuthUrl({
            access_type: 'offline',
            scope: ['https://www.googleapis.com/auth/spreadsheets.readonly'],
        });
        sh`open ${url}`;
        const rl = readline.createInterface({
            input: process.stdin,
        });
        rl.question('enter the token', (code) => {

        });
    }

    async create(name: string) {
        const sheetApi = google.sheets('v4');
    }
}