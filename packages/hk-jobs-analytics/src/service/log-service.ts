import { injectable } from 'inversify';
import { Subject } from 'rxjs/Subject';
import { SAHK_HFA } from '../interfaces';
import LogLevel = SAHK_HFA.Enums.LogLevel;
import Service = SAHK_HFA.Service;
import Logger = SAHK_HFA.Service.Logger;

@injectable()
export class LogService implements Service.LogService {
    $: Subject<{ level: SAHK_HFA.Enums.LogLevel; from: any; meta: any[]; args: any[] }>
        = new Subject();

    getLogger(from: any): SAHK_HFA.Service.Logger {
        function LogFunction(level: LogLevel) {
            return (...args) => this.$.next({ level, from, meta: [], args });
        }

        return ['info', 'debug', 'warn', 'error', 'trace'].reduce((pr, k) => {
            pr[k] = LogFunction.call(this, LogLevel[k]);
            return pr;
        }, {}) as Logger;
    }
}