import { injectable } from 'inversify';
import { resolve } from 'path';

@injectable()
export class PathService {
    sqlitePath: string = resolve(__dirname, '../../', 'db', 'default.sqlite');
}